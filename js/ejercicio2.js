const paises = [
    {
        "pais": "Estados Unidos",
        "moneda": "Dólar estadounidense",
        "valor_cambio": 1.00
    },
    {
        "pais": "España",
        "moneda": "Euro",
        "valor_cambio": 0.85
    },
    {
        "pais": "Japón",
        "moneda": "Yen",
        "valor_cambio": 110.62
    },
    {
        "pais": "Reino Unido",
        "moneda": "Libra esterlina",
        "valor_cambio": 0.74
    },
    {
        "pais": "Canadá",
        "moneda": "Dólar canadiense",
        "valor_cambio": 1.28
    },
    {
        "pais": "Australia",
        "moneda": "Dólar australiano",
        "valor_cambio": 1.36
    },
    {
        "pais": "China",
        "moneda": "Yuan",
        "valor_cambio": 6.37
    },
    {
        "pais": "India",
        "moneda": "Rupia india",
        "valor_cambio": 74.92
    },
    {
        "pais": "Brasil",
        "moneda": "Real brasileño",
        "valor_cambio": 5.35
    },
    {
        "pais": "México",
        "moneda": "Peso mexicano",
        "valor_cambio": 20.17
    },
    {
        "pais": "Alemania",
        "moneda": "Euro",
        "valor_cambio": 0.85
    },
    {
        "pais": "Francia",
        "moneda": "Euro",
        "valor_cambio": 0.85
    },
    {
        "pais": "Italia",
        "moneda": "Euro",
        "valor_cambio": 0.85
    },
    {
        "pais": "Rusia",
        "moneda": "Rublo ruso",
        "valor_cambio": 80.31
    },
    {
        "pais": "Sudáfrica",
        "moneda": "Rand",
        "valor_cambio": 14.88
    }
];

let range = document.querySelector('#range');
let inputs = document.querySelectorAll('input[type="text"]');
inputs[0].value = paises[1].pais;
inputs[1].value = paises[1].moneda;
inputs[2].value = paises[1].valor_cambio;

range.oninput = () => {
    inputs[0].value = paises[range.value].pais;
    inputs[1].value = paises[range.value].moneda;
    inputs[2].value = paises[range.value].valor_cambio;
}