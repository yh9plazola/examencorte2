let btnMostrar = document.querySelector('.btnMostrar');
let select = document.querySelector('#numeros');
let tabla = document.querySelector('.tabla-container');

const mostrar = (numero) => {
    tabla.innerHTML = "";
    for(let i = 1; i <= 10; i++){
        let fila = `<div class="fila">
        <img src="../resources/${numero}.png" class="n1">
        <img src="../resources/x.png" class="x">
        <img src="../resources/${i}.png" class="n2">
        <img src="../resources/=.png" class="equal">`;
        let num = `${numero * i}`;
        if(num < 10) {
            fila += `<img src="../resources/${numero * i}.png" class="equal">
            </div>`;
        }
        else if(num < 99) {
            fila += `<img src="../resources/${num[0]}.png" class="n1">
            <img src="../resources/${num[1]}.png" class="n2">
            </div>`;
        }
        else {
            fila += `<img src="../resources/${num[0]}.png" class="n1">
            <img src="../resources/${num[1]}.png" class="n2">
            <img src="../resources/${num[2]}.png" class="n1">
            </div>`;
        }
        tabla.innerHTML += fila;
    }
}

btnMostrar.onclick = () => {
    mostrar(select.value);
}